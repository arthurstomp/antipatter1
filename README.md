# README

```
$ bundle install
 
$ rake db:create db:migrate
 
$ rake db:seed

$ rails s

# open `localhost:3000`
```

What is wrong?

Can you identify what is happening?

Can you fix it?
