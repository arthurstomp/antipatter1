class ProductVariation < ApplicationRecord
  belongs_to :product
  enum coating: [ :metal, :wood, :plastic ]

  def reference
    "#{color} - #{coating}"
  end
end
