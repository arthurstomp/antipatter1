class CreateProductVariations < ActiveRecord::Migration[5.2]
  def change
    create_table :product_variations do |t|
      t.references :product
      t.string :color
      t.integer :coating

      t.timestamps
    end
  end
end
