# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

100.times do
  prod = Product.new({
    name: Faker::Games::Pokemon.name,
    price: rand(100000)
  })
  prod.save!

  10.times do
    pv = ProductVariation.new({
      product: prod,
      color: Faker::Color.hex_color,
      coating: rand(2)
    })
    pv.save!
  end
end
